import React from 'react';
import { ErrorMessage } from 'formik';
import * as Yup from 'yup';

export const validationSchemaForForm = Yup.object({
    fname: Yup.string().required('Please Provide Your First Name'),
    lname: Yup.string().required('Please Provide Your Last Name'),
    email: Yup.string().email('Enter Valid Email').required('Please Provide Your Email'),
    gender: Yup.string().required('Please Select Your Gender'),
    specialization: Yup.string().required('Please Select Your specialization'),
});

export const validationSchemaForLogin = Yup.object({
    email: Yup.string().email('Enter Valid Email').required('Please Provide Your Email'),
    password: Yup.string().required('Please Enter Your Password'),
});

export const ErrorMSG = (error) => {
    return (
        <div className="text-danger">
            <ErrorMessage name={error}></ErrorMessage>
        </div>
    );
};

export const uniqIdGenerator = () => {
    return Date.now();
};
