export const selectOptions = [
    { key: 'Select Please Select Your Specialization ', value: '' },
    { key: 'Front-end Developer', value: 'Front-end Developer' },
    { key: 'Back-end Developer', value: 'Back-end Developer' },
    { key: 'Full-stack Developer', value: 'Full-stack Developer' },
];

export const users = [
    {
        id: '1656589191099',
        fname: 'Divyang',
        lname: 'Patel',
        email: 'patel2divyang@gmail.com',
        gender: 'Male',
        specialization: 'Front-end Developer',
    },
    {
        id: '1656589191288',
        fname: 'Krishna',
        lname: 'Rudani',
        email: 'krishna@gmail.com',
        gender: 'Female',
        specialization: 'Front-end Developer',
    },
    {
        id: '1656568191277',
        fname: 'Roger',
        lname: 'Baldwin',
        email: 'roger.baldwin.cis@gmail.com',
        gender: 'Male',
        specialization: 'Full-stack Developer',
    },
];
