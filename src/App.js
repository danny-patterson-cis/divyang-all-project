import React from 'react';
import './App.css';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Login from './components/Login';
import { UsersData } from './components/UsersData';
import PrivateRoute from './privateRouting/privateRouting';

function App() {
    return (
        <div className="App">
            <Router>
                <Switch>
                    <PrivateRoute component={UsersData} path="/users-list" />
                    <Route component={Login} path="/" />
                </Switch>
            </Router>
        </div>
    );
}

export default App;
