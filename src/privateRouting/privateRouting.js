import React from 'react';
import { Redirect, Route } from 'react-router-dom';

// eslint-disable-next-line react/prop-types
const PrivateRoute = ({ component: components, ...props }) => {
    const token = localStorage.getItem('Email');

    if (token) {
        return <Route component={components} {...props} />;
    }
    return <Redirect to="/" path="/" />;
};

export default PrivateRoute;
