import React, { useEffect, useState } from 'react';
import { Formik, Field, Form } from 'formik';
import { validationSchemaForLogin as validationSchema } from '../modules/utils/utils';
import { useHistory } from 'react-router-dom';
import { ErrorMSG } from '../modules/utils/utils';

const Login = () => {
    const [alertMSG, setAlertMSG] = useState(false);
    const history = useHistory();
    const token = localStorage.getItem('Email');
    const initialValues = {
        email: '',
        password: '',
    };

    const onSubmit = (loginValues) => {
        if (loginValues.email === 'admin@demo.com' && loginValues.password === 'Admin@123') {
            localStorage.setItem('Email', JSON.stringify(loginValues.email));
            history.push('/users-list');
        } else {
            setAlertMSG(true);
        }
    };

    useEffect(() => {
        if (token) {
            history.push('/users-list');
        }
    });

    return (
        <Formik initialValues={initialValues} onSubmit={onSubmit} validationSchema={validationSchema}>
            {() => {
                return (
                    <section className="text-center">
                        <div
                            className="backGround p-5 bg-image"
                            style={{
                                backgroundImage: "url('https://mdbootstrap.com/img/new/textures/full/171.jpg')",
                                height: '300px',
                            }}
                        >
                            {alertMSG ? (
                                <div className="alert alert-danger" role="alert">
                                    Please Enter Valid Login!
                                </div>
                            ) : (
                                ''
                            )}
                        </div>
                        <div
                            className="card mx-4 mx-md-5 shadow-5-strong"
                            style={{
                                marginTop: '-100px',
                                background: 'hsla(0, 0%, 100%, 0.8)',
                                backdropFilter: 'blur(30px)',
                            }}
                        >
                            <div className="card-body py-5 px-md-5">
                                <div className="row d-flex justify-content-center">
                                    <div className="col-lg-8">
                                        <h2 className="fw-bold mb-5">Login Now</h2>
                                        <Form>
                                            <div className="form-outline mb-4">
                                                <label className="form-label" htmlFor="em">
                                                    Email address
                                                </label>
                                                <Field type="email" id="email" name="email" className="form-control" />
                                                {ErrorMSG('email')}
                                            </div>
                                            <div className="form-outline mb-3">
                                                <label className="form-label" htmlFor="pw">
                                                    Password
                                                </label>
                                                <Field type="password" id="password" name="password" className="form-control" />
                                                {ErrorMSG('password')}
                                            </div>
                                            <button type="submit" className="btn btn-primary btn-block mb-3">
                                                Login
                                            </button>
                                            <div className="text-center">
                                                <p>
                                                    login with: <br></br>
                                                    Email: admin@demo.com<br></br>
                                                    Password: Admin@123
                                                </p>
                                            </div>
                                        </Form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                );
            }}
        </Formik>
    );
};

export default Login;
