/* eslint-disable react/prop-types */
import React from 'react';
import { Modal, ModalBody, ModalHeader } from 'reactstrap';
import { useSelector, useDispatch } from 'react-redux';
import { Formik, Form, Field } from 'formik';
import { addUser } from '../redux/actions/addUserAction';
import { editedUser } from '../redux/actions/editedUserAction';
import { validationSchemaForForm as validationSchema, uniqIdGenerator, ErrorMSG } from '../modules/utils/utils';
import { selectOptions } from '../modules/data/data';
import '../assets/styles/userForm.css';
// eslint-disable-next-line react/prop-types

export const UserForm = ({ open, setOpen, editInputValue, setEditInputValue }) => {
    const dispatch = useDispatch();
    const initialValues = {
        id: editInputValue ? editInputValue?.id : '',
        fname: editInputValue ? editInputValue?.fname : '',
        lname: editInputValue ? editInputValue?.lname : '',
        email: editInputValue ? editInputValue?.email : '',
        gender: editInputValue ? editInputValue?.gender : '',
        specialization: editInputValue ? editInputValue?.specialization : '',
    };
    const users = useSelector((state) => state.stateUsers.users);

    const onSubmit = (values, { resetForm }) => {
        if (!initialValues.id) {
            let addDataWithId = { ...values, id: uniqIdGenerator() };
            dispatch(addUser(addDataWithId));
            setOpen(false);
            resetForm({ values: '' });
        } else {
            const updateData = users.map((d) => {
                if (d.id === values.id) {
                    return values;
                }
                return d;
            });
            dispatch(editedUser(updateData));
            resetForm({ values: '' });
            setEditInputValue('');
            setOpen(false);
        }
    };

    const closeButton = (e) => {
        e.preventDefault(e);
        setOpen(false);
        setEditInputValue('');
    };

    return (
        <Formik initialValues={initialValues} onSubmit={onSubmit} validationSchema={validationSchema} enableReinitialize={true}>
            {() => {
                return (
                    <Modal fullscreen="lg" size="lg" isOpen={open}>
                        <ModalHeader className="modalHeader" charCode="Y" toggle={closeButton} />
                        <ModalBody className="modalBody">
                            <h5 className="text-center mb-2">{editInputValue ? `Edit ${editInputValue.fname} Data` : 'Add New User'}</h5>
                            <Form className="d-flex flex-column justify-center mt-3">
                                <div className="mb-3 text-center text-dark">
                                    <label htmlFor="exampleInputEmail1" className="form-label">
                                        First Name
                                    </label>
                                    <Field type="text" className="form-control" name="fname" id="fname" placeholder="Your First Name" />
                                    {ErrorMSG('fname')}
                                </div>
                                <div className="mb-3 text-center text-dark">
                                    <label htmlFor="exampleInputPassword1" className="form-label">
                                        Last Name
                                    </label>
                                    <Field type="text" className="form-control" name="lname" id="lname" placeholder="Your Last Name" />
                                    {ErrorMSG('lname')}
                                </div>
                                <div className="mb-3 text-center text-dark">
                                    <label htmlFor="exampleFormControlInput1" className="form-label">
                                        Email address
                                    </label>
                                    <Field type="email" className="form-control" name="email" id="email" placeholder="name@example.com" />
                                    {ErrorMSG('email')}
                                </div>
                                <fieldset className="row mb-3 text-dark">
                                    <div className="col-sm-10">
                                        <div className="col-form-label  ">
                                            <div className="ms-5 text-center">Please Select Your Gender</div>
                                            <div className="form-check">
                                                <Field className="form-check-input" type="radio" label="Male" name="gender" id="Male" value="Male" />
                                                <label className="form-check-label" htmlFor="Male">
                                                    Male
                                                </label>
                                            </div>
                                            <div className="form-check">
                                                <Field className="form-check-input" type="radio" label="Female" name="gender" id="Female" value="Female" />
                                                <label className="form-check-label" htmlFor="Female">
                                                    Female
                                                </label>
                                            </div>
                                            {ErrorMSG('gender')}
                                        </div>
                                    </div>
                                </fieldset>
                                <Field as="select" className="form-select" aria-label="Default select example" name="specialization" id="specialization">
                                    {selectOptions.map((option) => {
                                        return (
                                            <option key={option.value} value={option.value}>
                                                {option.key}
                                            </option>
                                        );
                                    })}
                                </Field>
                                {ErrorMSG('specialization')}
                                <div className="modalFooter mb-4">
                                    <button type="submit" className="btn btn-primary mt-3">
                                        {editInputValue ? 'Edit' : 'Add User'}
                                    </button>
                                </div>
                            </Form>
                        </ModalBody>
                    </Modal>
                );
            }}
        </Formik>
    );
};
