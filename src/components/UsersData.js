import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import UsersListing from './UsersListing';
import '../assets/styles/userData.css';

export const UsersData = () => {
    const history = useHistory();
    const [open, setOpen] = useState(false);

    const logout = (e) => {
        e.preventDefault();
        localStorage.removeItem('Email');
        history.push('/');
    };

    return (
        <div>
            <div className="addUser mt-3 mb-5 ms-3">
                <button className="btn btn-primary" onClick={() => setOpen(true)}>
                    Add User
                </button>
            </div>
            <div className="logout mt-3 mb-5 me-3">
                <button className="btn btn-danger" onClick={logout}>
                    Logout
                </button>
            </div>
            <UsersListing open={open} setOpen={setOpen} />
        </div>
    );
};
