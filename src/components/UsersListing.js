import React, { useEffect, useState } from 'react';
import { Modal, ModalBody, ModalHeader, ModalFooter } from 'reactstrap';
import { deleteUser } from '../redux/actions/deleteUserAction';
import { useSelector, useDispatch } from 'react-redux';
import { getUser } from '../redux/actions/getUsersAction';
import { UserForm } from './UserForm';

// eslint-disable-next-line react/prop-types
const UsersListing = ({ setOpen, open }) => {
    const [deleteAlert, setDeleteAlert] = useState(false);
    const [editInputValue, setEditInputValue] = useState();
    const [deleteData, setDeleteData] = useState();
    const dispatch = useDispatch();
    const users = useSelector((state) => state.stateUsers.users);

    useEffect(() => {
        dispatch(getUser);
    }, [dispatch]);

    const editFunction = (d) => {
        setEditInputValue(d);
        setOpen(true);
    };

    const deleteButton = (d) => {
        setDeleteAlert(true);
        setDeleteData(d);
    };

    const deleteFunction = () => {
        const deleted = users.filter((d) => {
            return d.id !== deleteData.id;
        });
        dispatch(deleteUser(deleted));
        dispatch(getUser());
        setDeleteAlert(false);
    };
    return (
        <div>
            <table className="table table-dark table-striped mt-5">
                <thead>
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">First Name</th>
                        <th scope="col">Last Name</th>
                        <th scope="col">Email</th>
                        <th scope="col">Gender</th>
                        <th scope="col">Specialization</th>
                        <th scope="col">Edit</th>
                        <th scope="col">Delete</th>
                    </tr>
                </thead>
                <tbody>
                    {users &&
                        users.map((d) => (
                            <tr className="table table-striped-active" key={d.id}>
                                <th scope="row">{d.id}</th>
                                <td>{d.fname}</td>
                                <td>{d.lname}</td>
                                <td>{d.email}</td>
                                <td>{d.gender}</td>
                                <td>{d.specialization}</td>
                                <td>
                                    <button type="button" className="btn btn-primary" onClick={() => editFunction(d)}>
                                        Edit
                                    </button>
                                </td>
                                <td>
                                    <button type="button" className="btn btn-danger " onClick={() => deleteButton(d)}>
                                        Delete
                                    </button>
                                </td>
                            </tr>
                        ))}
                </tbody>
            </table>
            <div>
                <Modal isOpen={deleteAlert}>
                    <ModalHeader className="text-danger" toggle={() => setDeleteAlert(false)}>
                        Warning!
                    </ModalHeader>
                    <ModalBody>{`Are You Sure You Want to Delete user "${`${deleteData?.fname}`.toUpperCase()}" Data ?`}</ModalBody>
                    <ModalFooter className="border-0 mb-2">
                        <button className="btn btn-danger" onClick={deleteFunction}>
                            Delete
                        </button>{' '}
                        <button className="btn btn-secondary" onClick={() => setDeleteAlert(false)}>
                            Cancel
                        </button>
                    </ModalFooter>
                </Modal>
            </div>
            <UserForm editInputValue={editInputValue} setOpen={setOpen} open={open} setEditInputValue={setEditInputValue} />
        </div>
    );
};

export default UsersListing;
