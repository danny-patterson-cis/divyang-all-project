import { users } from '../../modules/data/data';
import * as types from '../types/types';

const initialState = {
    users: users,
};

export const userReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.GET_USERS:
            return {
                ...state,
            };
        case types.ADD_USER:
            return {
                ...state,
                users: [...state.users, action.data],
            };
        case types.DELETE_USER:
            return {
                users: action.data,
            };
        case types.UPDATE_USERS_DATA:
            return {
                users: action.data,
            };
        default:
            return state;
    }
};
