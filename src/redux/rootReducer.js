import { combineReducers } from 'redux';
import { userReducer } from './reducer/userReducer';

const rootReducer = combineReducers({
    stateUsers: userReducer,
});

export default rootReducer;
