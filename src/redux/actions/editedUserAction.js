import * as types from '../types/types';

export const editedUser = (user) => {
    return {
        type: types.UPDATE_USERS_DATA,
        data: user,
    };
};
