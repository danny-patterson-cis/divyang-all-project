import * as types from '../types/types';

export const deleteUser = (users) => {
    return {
        type: types.DELETE_USER,
        data: users,
    };
};
