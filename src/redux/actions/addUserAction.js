import * as types from '../types/types';

export const addUser = (users) => {
    return {
        type: types.ADD_USER,
        data: users,
    };
};
